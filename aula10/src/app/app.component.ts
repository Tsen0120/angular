import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'aula10';
  valorAtual = '';
  valorSalvo = '';
  isMouseOver = false;
  nomeDoCurso = 'Angular';
  pessoa: any = {
    nome: 'Jon',
    idade: 20
  };

  botaoClicado() {
    alert('Botão Clicado');
  }



  onMouseOverOut() {
    this.isMouseOver = !this.isMouseOver;
  }


//   onKeyUp(evento: KeyboardEvent) {
//     this.valorAtual = evento.target.value;
//   }
//   salvarValor(valor) {
//     this.valorSalvo = valor;
//   }
}

