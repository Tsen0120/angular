import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-contador',
  templateUrl: './output.component.html',
  styleUrls: ['./output.component.css']
})
export class OutputComponent implements OnInit {

  valor = 0;

  adiciona(){
    this.valor++;
  }

  subitrai(){
    this.valor--;
  }

  constructor() { }

  ngOnInit(): void {
  }

}
