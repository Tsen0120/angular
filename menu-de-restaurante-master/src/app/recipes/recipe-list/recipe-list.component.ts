import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { Recipe } from '../recipe.model';

@Component({
  selector: 'app-recipe-list',
  templateUrl: './recipe-list.component.html',
  styleUrls: ['./recipe-list.component.css']
})
export class RecipeListComponent implements OnInit {
  @Output() recepieWasSelected= new EventEmitter<Recipe>();
  recipe: Recipe[] = [
    new Recipe('Primeira Opção ', 'Sanduiche de carne', 'https://abrilclaudia.files.wordpress.com/2019/07/sanduicc81che-de-carne-louca.jpg?quality=85&strip=info&resize=680,453'),
    new Recipe('Segunda Opção', 'Carne e batata', 'https://upload.wikimedia.org/wikipedia/commons/d/d1/Typical_food_of_eastern_Venezuela_-_Comida_t%C3%ADpica_del_oriente_Venezolano.JPG')

  ];
  constructor() { }

  ngOnInit(): void {
  }

  onRecipeSelected(recipe: Recipe){
    this.recepieWasSelected.emit(recipe);
  }

}
